import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/range';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/defaultIfEmpty';
import { Board } from '../../../psssrvr/board';
import * as io from 'socket.io-client';
import Socket = SocketIOClient.Socket;

export enum Status { BlueTurn, RedTurn, BlueWin, RedWin, Tie }

export interface State {
  status: Status;
  board: Board;
}

export class Play {
  constructor(public readonly column: number) {
  }
}

export class Reset {
}

export type Action = Play | Reset;

@Injectable()
export class StateService {

  private managedState: BehaviorSubject<State>;
  public observable: Observable<State>;
  private socket: Socket;

  constructor() {
    const state: State = {status: Status.BlueTurn, board: new Board()};

    this.managedState = new BehaviorSubject(state);
    this.observable = this.managedState.asObservable();

    this.socket = io('http://localhost:8081');
    this.socket.on('state', (data) => {
      this.managedState.next({status: data.status, board: Board.boardFrom(data.board)});
    });
  }

  public dispatch(action: Action): void {
    if (action instanceof Play) {
      this.socket.emit('play', action.column);
    }
    if (action instanceof Reset) {
      this.socket.emit('reset');
    }
  }
}
