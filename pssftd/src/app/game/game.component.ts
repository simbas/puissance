import { Component, OnInit } from '@angular/core';
import { Action, StateService, Status } from '../state.service';
import { Board } from '../../../../psssrvr/board';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  public board: Board;
  public status: Status;

  constructor(private stateService: StateService) {
  }

  ngOnInit() {
    this.stateService.observable.subscribe(state => {
      this.board = state.board;
      this.status = state.status;
    });
  }

  public dispatch(a: Action): void {
    this.stateService.dispatch(a);
  }
}
