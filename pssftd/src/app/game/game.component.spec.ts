import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GameComponent } from './game.component';
import { Subject } from 'rxjs/Subject';
import { State, StateService, Status } from '../state.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Board } from '../../../../psssrvr/board';
import { Empty } from '../../../../psssrvr/cell';

describe('GameComponent', () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;
  let stateSubject;
  let stateService;

  beforeEach(async(() => {
    stateSubject = new Subject<State>();
    stateService = {
      observable: stateSubject.asObservable()
    };

    TestBed.configureTestingModule({
      declarations: [GameComponent],
      providers: [{provide: StateService, useValue: stateService}],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should observe stateService', () => {
    expect(component.board).toBeUndefined();
    stateSubject.next({status: Status.BlueTurn, board: new Board()});
    expect(component.board.columns).toBe(7);
    expect(component.board.rows).toBe(6);
    expect(component.board.get(5, 2) instanceof Empty).toBeTruthy();
  });
});
