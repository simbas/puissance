import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { AtomKind, Cell } from '../../../../psssrvr/cell';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CellComponent {
  @Input()
  public cell: Cell;

  public getCellClass(): String {
    switch (this.cell.kind) {
      case AtomKind.Blue:
        return 'blue';
      case AtomKind.Red:
        return 'red';
      case AtomKind.Empty:
        return 'empty';
      case AtomKind.Nothing:
        return 'nothing';
    }
  }
}
