import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CellComponent } from './cell.component';
import { Blue, Empty, Nothing, Red } from '../../../../psssrvr/cell';

describe('CellComponent', () => {
  let component: CellComponent;
  let fixture: ComponentFixture<CellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CellComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CellComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    component.cell = new Empty();
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  describe('getCellClass', () => {
    it('should return blue when cell is Blue', () => {
      component.cell = new Blue();
      fixture.detectChanges();
      expect(component.getCellClass()).toBe('blue');
    });
    it('should return empty when cell is Empty', () => {
      component.cell = new Empty();
      fixture.detectChanges();
      expect(component.getCellClass()).toBe('empty');
    });
    it('should return red when cell is Red', () => {
      component.cell = new Red();
      fixture.detectChanges();
      expect(component.getCellClass()).toBe('red');
    });
    it('should return nothing when cell is Nothing', () => {
      component.cell = new Nothing();
      fixture.detectChanges();
      expect(component.getCellClass()).toBe('nothing');
    });
  });
});
