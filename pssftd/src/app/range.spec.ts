import { range } from './range';

describe('range', () => {
  it('should returns an array [0...4] when 5', () => {
    const rangedArray = range(5);
    expect(rangedArray.length).toBe(5);
    expect(rangedArray[0]).toBe(0);
    expect(rangedArray[1]).toBe(1);
    expect(rangedArray[4]).toBe(4);
  });
});
