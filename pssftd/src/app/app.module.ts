import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BoardComponent } from './board/board.component';
import { StateService } from './state.service';
import { GameComponent } from './game/game.component';
import { HeadComponent } from './head/head.component';
import { CellComponent } from './cell/cell.component';

@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    GameComponent,
    HeadComponent,
    CellComponent
  ],
  imports: [
    FormsModule,
    BrowserModule
  ],
  providers: [StateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
