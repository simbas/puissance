import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Play, Reset, Status } from '../state.service';
import { range } from '../range';

@Component({
  selector: 'app-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeadComponent {
  @Input()
  public status: Status;

  @Input()
  public limit: number;

  @Output()
  public onPlayed = new EventEmitter<Play>();

  @Output()
  public onReset = new EventEmitter<Reset>();

  public getStatusLabel(status: Status): String {
    switch (status) {
      case Status.BlueTurn:
        return '';
      case Status.RedTurn:
        return '';
      case Status.BlueWin:
        return ' - Blue wins!';
      case Status.RedWin:
        return ' - Red wins!';
      case Status.Tie:
        return ' - Tie';
    }
  }

  public getRowClass(status: Status): String {
    switch (status) {
      case Status.BlueTurn:
        return 'blue';
      case Status.RedTurn:
        return 'red';
      case Status.BlueWin:
        return 'hidden';
      case Status.RedWin:
        return 'hidden';
      case Status.Tie:
        return 'hidden';
    }
  }

  public play(column: number) {
    this.onPlayed.emit(new Play(column));
  }
  public reset() {
    this.onReset.emit(new Reset());
  }

  public range(limit: number = 0): Array<number> {
    return range(limit);
  }
}
