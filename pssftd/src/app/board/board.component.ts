import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Board } from '../../../../psssrvr/board';
import { range } from '../range';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BoardComponent {
  @Input()
  public board: Board;

  public range(limit: number = 0): Array<number> {
    return range(limit);
  }
}
