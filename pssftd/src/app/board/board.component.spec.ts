import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BoardComponent } from './board.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Board } from '../../../../psssrvr/board';
import { Empty } from '../../../../psssrvr/cell';

describe('BoardComponent', () => {
  let component: BoardComponent;
  let fixture: ComponentFixture<BoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BoardComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardComponent);
    component = fixture.componentInstance;
    component.board = new Board();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a board', () => {
    expect(component.board.columns).toBe(7);
    expect(component.board.rows).toBe(6);
    expect(component.board.get(5, 2) instanceof Empty).toBeTruthy();
  });

});
