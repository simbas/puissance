import { AtomKind, Blue, Cell, Empty, Nothing, Red } from './cell';

export interface Array2D<T> {
    rows: number;
    columns: number;

    get(row: number, column: number): T;

    set(row: number, column: number, val: T): Array2D<T>;

    toString(): string;
}

export class Board implements Array2D<Cell> {
    public readonly columns = 7;
    public readonly rows = 6;

    constructor(private array?: Array<Cell>) {
        if (typeof this.array === 'undefined') {
            this.array = new Array<Cell>(this.columns * this.rows);
            this.array.fill(new Empty());
        }
    }

    static boardFrom(rep: string): Board {
        rep = rep.split('\n').reverse().join('\n');
        rep = rep.replace(/\s/g, '');
        rep = rep.replace(/\n/g, '');
        const array = rep.split('').map(cell => {
            switch (cell) {
                case 'B':
                    return new Blue();
                case 'R':
                    return new Red();
                case 'o':
                    return new Empty();
                default:
                    return new Empty();
            }
        });
        return new Board(array);
    }

    get(row: number, column: number): Cell {

        const index = row * this.columns + column;
        if (index < 0 || index >= this.array!.length) {
            return new Nothing();
        }
        return this.array![index];
    }

    getRow(row: number): Array<Cell> {
        if (row < 0 || row >= this.rows) {
            return [];
        }
        return this.array!.slice(row * this.columns, (row + 1) * this.columns);
    }

    getColumn(column: number): Array<Cell> {
        if (column < 0 || column >= this.columns) {
            return [];
        }
        return this.array!.filter((cell, index) => (index - column) % this.columns === 0);
    }

    getNEDiagonal(row: number, column: number): Array<Cell> {
        if (row < 0 || row >= this.rows) {
            return [];
        }
        if (column < 0 || column >= this.columns) {
            return [];
        }
        return this.array!.filter((cell, index) => {
            const currentRow = Math.floor(index / this.columns);
            const currentColumn = Math.floor(index % this.columns);
            return (column - currentColumn) === (row - currentRow);
        });
    }

    getNWDiagonal(row: number, column: number): Array<Cell> {
        if (row < 0 || row >= this.rows) {
            return [];
        }
        if (column < 0 || column >= this.columns) {
            return [];
        }
        return this.array!.filter((cell, index) => {
            const currentRow = Math.floor(index / this.columns);
            const currentColumn = Math.floor(index % this.columns);
            const diff = (row - currentRow);
            return currentColumn === (column + diff);
        });
    }

    set(row: number, column: number, val: Cell): Board {
        const newArray = this.array!.slice(0);
        const index = row * this.columns + column;
        newArray[index] = val;
        return new Board(newArray);
    }

    isFull(): boolean {
        return this.array!.every(el => !(el instanceof Empty));
    }

    toString(): string {
        const strRep = this.array!.reduce((str, cell, index) => {
            let char: string = '';

            switch (cell.kind) {
                case AtomKind.Blue:
                    char = 'B';
                    break;
                case AtomKind.Red:
                    char = 'R';
                    break;
                case AtomKind.Empty:
                    char = 'o';
                    break;
                case AtomKind.Nothing:
                    char = 'n';
                    break;
            }
            return `${str}${char}${(index + 1) % this.columns === 0 ? '\n' : ' '}`;
        }, '\n');

        return strRep.split('\n').reverse().join('\n');
    }
}
