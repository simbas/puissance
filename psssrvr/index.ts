import { Play, Reset, State, StateService, Status } from './state';
import Socket = SocketIO.Socket;

const io = require('socket.io')(8081);

const state = new StateService();

io.on('play', (column: number) => {
    state.dispatch(new Play(column));
});

io.on('reset', () => {
    state.dispatch(new Reset());
});

let currentState: State;

function serialize(stt: State): { status: Status, board: string } {
    return {status: stt.status, board: stt.board.toString()}
}

io.on('connection', (socket: Socket) => {
    io.emit('state', serialize(currentState));
    socket.on('reset', () => state.dispatch(new Reset()));
    socket.on('play', (column: number) => state.dispatch(new Play(column)));
});

state.observable.subscribe(newState => {
    currentState = newState;
    console.log(`-------\n${currentState.board.toString()}`);
    io.emit('state', serialize(newState));
});
