export enum AtomKind { Blue, Red, Empty, Nothing }


export interface Atom {
  kind: AtomKind;
}

export class Blue implements Atom {
  public kind = AtomKind.Blue;
}

export class Red implements Atom {
  public kind = AtomKind.Red;
}

export class Empty implements Atom {
  public kind = AtomKind.Empty;
}

export class Nothing implements Atom {
  public kind = AtomKind.Nothing;
}

export type Cell = Blue | Red | Empty | Nothing;
