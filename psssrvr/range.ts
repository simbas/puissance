export function range(limit: number): Array<number> {
  return Array<number>(limit).fill(0).map((x, i) => i);
}

