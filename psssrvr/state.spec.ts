import { Play, Reset, StateService, Status } from './state';
import { Board } from './board';
import { expect } from 'chai';
import { boardFrom } from './board.spec';

describe('StateService', () => {
    let service: StateService;
    beforeEach(() => {
        service = new StateService();
    });


    it('should init a first state (blue turn, empty board)', () => {
        const expected = `
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
`;
        service.observable.subscribe(state => {
            expect(state.status).equal(Status.BlueTurn);
            expect(state.board.toString()).equal(expected);
        });
    });

    describe('update', () => {
        it('should return (one played board, red turn) when (play, empty board)', () => {
            const expected = `
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o B o o o o o
`;
            service.update(new Play(1), {status: Status.BlueTurn, board: new Board()}).subscribe(state => {
                expect(state.status).equal(Status.RedTurn);
                expect(state.board.toString()).equal(expected);
            });
        });

        it('should return (two played board, red turn) when (play, one played board)',
            () => {

                const initial = `
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o B o o o o o
`;
                const expected = `
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o R o o o o o
o B o o o o o
`;
                service.update(new Play(1), {status: Status.RedTurn, board: boardFrom(initial)}).subscribe(state => {
                    expect(state.status).equal(Status.BlueTurn);
                    expect(state.board.toString()).equal(expected);
                });
            });

        it('should return (blue won board, blue win) when (play, near horizontal won board)',
            () => {
                const initial = `
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
R R R o o o o
B B B o o o o
`;
                const expected = `
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
R R R o o o o
B B B B o o o
`;
                service.update(new Play(3), {status: Status.BlueTurn, board: boardFrom(initial)}).subscribe(state => {
                    expect(state.status).equal(Status.BlueWin);
                    expect(state.board.toString()).equal(expected);
                });
            });

        it('should return (red won board, red win) when (play, near vertical won board)',
            () => {
                const initial = `
o o o o o o o
o o o o o o o
o o o o o o o
o R B o o o o
o R B o o o o
o R B B o o o
`;
                const expected = `
o o o o o o o
o o o o o o o
o R o o o o o
o R B o o o o
o R B o o o o
o R B B o o o
`;
                service.update(new Play(1), {status: Status.RedTurn, board: boardFrom(initial)}).subscribe(state => {
                    expect(state.status).equal(Status.RedWin);
                    expect(state.board.toString()).equal(expected);
                });
            });

        it('should return (blue won board, blue win) when (play, near NE diagonal won board)',
            () => {
                const initial = `
o o o o o o o
o o o o o B R
o o o o B R B
o o o B R B R
o o o R B B B
o o o B R R R
`;
                const expected = `
o o o o o o B
o o o o o B R
o o o o B R B
o o o B R B R
o o o R B B B
o o o B R R R
`;
                service.update(new Play(6), {status: Status.BlueTurn, board: boardFrom(initial)}).subscribe(state => {
                    expect(state.status).equal(Status.BlueWin);
                    expect(state.board.toString()).equal(expected);
                });
            });

        it('should return (red won board, red win) when (play, near NW diagonal won board)',
            () => {
                const initial = `
o o o o o o o
o o o o o o o
o o o o o o o
o o o B R B R
o o o R B R B
o o o B R R R
`;
                const expected = `
o o o o o o o
o o o o o o o
o o o R o o o
o o o B R B R
o o o R B R B
o o o B R R R
`;
                service.update(new Play(3), {status: Status.RedTurn, board: boardFrom(initial)}).subscribe(state => {
                    expect(state.status).equal(Status.RedWin);
                    expect(state.board.toString()).equal(expected);
                });
            });

        it('should reset the game when reset action',
            () => {
                const initial = `
o o o o o o o
o o o o o o o
o o o o o o o
o o o B R B R
o o o R B R B
o o o B R R R
`;
                const expected = `
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
`;
                service.update(new Reset(), {status: Status.BlueTurn, board: boardFrom(initial)}).subscribe(state => {
                    expect(state.status).equal(Status.BlueTurn);
                    expect(state.board.toString()).equal(expected);
                });
            });
    });
});
