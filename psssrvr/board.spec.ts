import { Board } from './board';
import { Blue, Empty, Nothing, Red } from './cell';
import { expect } from 'chai';

export const boardFrom = function (rep: string): Board {
    rep = rep.split('\n').reverse().join('\n');
    rep = rep.replace(/\s/g, '');
    rep = rep.replace(/\n/g, '');
    const array = rep.split('').map(cell => {
        switch (cell) {
            case 'B':
                return new Blue();
            case 'R':
                return new Red();
            case 'o':
                return new Empty();
            default:
                return new Nothing();
        }
    });
    return new Board(array);
};

describe('Board', () => {
    let board: Board;

    it('should have 6 row and 7 columns', () => {
        board = new Board();
        expect(board.columns).equal(7);
        expect(board.rows).equal(6);
    });

    it('should return an empty table when toString', () => {
        board = new Board();
        const expected = `
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
`;
        expect(board.toString()).equal(expected);
    });

    it('should return a table with one play when toString', () => {
        board = new Board();
        board = board.set(0, 2, new Blue());

        const expected = `
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o o B o o o o
`;
        expect(board.toString()).equal(expected);
    });

    it('should construct a board from given array', () => {
        const expected = `
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o o o o o o o
o o B R o o o
`;
        board = boardFrom(expected);

        expect(board.toString()).equal(expected);
    });

    it('should extract a given row', () => {
        const testedBoard = `
o o o o o o o
o o o o o o o
o o o o o o o
B B R R R B B
o o o o o o o
o o B R o o o
`;
        board = boardFrom(testedBoard);

        expect(board.getRow(2)).deep.equal(
            [new Blue(), new Blue(), new Red(), new Red(), new Red(), new Blue(), new Blue()]
        );
    });

    it('should extract a given column', () => {
        const testedBoard = `
o o o o o o o
o o o o o o o
o o o o o o o
B B R R R B B
o o o o o o o
o o B R o o o
`;
        board = boardFrom(testedBoard);

        expect(board.getColumn(2)).deep.equal(
            [new Blue(), new Empty(), new Red(), new Empty(), new Empty(), new Empty()]
        );
    });

    it('should extract a given NE diagonal', () => {
        const testedBoard = `
o o o o o o o
o o o o o o o
o o o o o o o
B B R R R B B
o o o o o o o
o o B R o o o
`;
        board = boardFrom(testedBoard);

        expect(board.getNEDiagonal(4, 2)).deep.equal(
            [new Blue(), new Empty(), new Empty(), new Empty()]
        );
    });
    it('should extract a given NW diagonal', () => {
        const testedBoard = `
o o o o o o o
o o o o o R o
o o o o o o o
B B R R R B B
o o o o o o o
o o B R o o o
`;
        board = boardFrom(testedBoard);

        expect(board.getNWDiagonal(4, 5)).deep.equal(
            [new Empty(), new Red(), new Empty()]
        );
    });

});
