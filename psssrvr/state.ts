import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/range';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/defaultIfEmpty';
import { Board } from './board';
import { Blue, Cell, Empty, Red } from './cell';

export enum Status { BlueTurn, RedTurn, BlueWin, RedWin, Tie }

export interface State {
    status: Status;
    board: Board;
}

export class Play {
    constructor(public readonly column: number) {
    }
}

export class Reset {
}

export type Action = Play | Reset;

export class StateService {

    private managedState: BehaviorSubject<State>;
    public observable: Observable<State>;

    constructor() {
        const state: State = {status: Status.BlueTurn, board: new Board()};

        this.managedState = new BehaviorSubject(state);
        this.observable = this.managedState.asObservable();
    }

    public dispatch(action: Action): void {
        const state = this.managedState.getValue();

        this.update(action, state)
            .subscribe(newState => this.managedState.next(newState));
    }

    public update(action: Action, state: State): Observable<State> {
        if (action instanceof Play) {
            return Observable.range(0, state.board.rows)
                .filter(row => state.board.get(row, action.column) instanceof Empty)
                .take(1)
                .flatMap(row => this.play(state, row, action.column))
                .defaultIfEmpty(state);
        }
        if (action instanceof Reset) {
            return Observable.of({status: Status.BlueTurn, board: new Board()});
        }
        return Observable.of(state);
    }

    private play(state: State, row: number, column: number): Observable<State> {
        let piece: Blue | Red;
        if (state.status === Status.BlueTurn) {
            piece = new Blue();
        } else if (state.status === Status.RedTurn) {
            piece = new Red();
        } else {
            return Observable.of(state);
        }

        const board = state.board.set(row, column, piece);

        if (board.isFull()) {
            return Observable.of({board, status: Status.Tie});
        }

        return this.checkAroundForAWin(board, row, column, piece)
            .filter(val => val)
            .map(_ => ({board, status: piece instanceof Blue ? Status.BlueWin : Status.RedWin}))
            .defaultIfEmpty({board, status: piece instanceof Blue ? Status.RedTurn : Status.BlueTurn});
    }

    private checkAroundForAWin(board: Board, row: number, column: number, piece: Blue | Red): Observable<boolean> {
        return Observable.from([board.getColumn(column), board.getRow(row), board.getNEDiagonal(row, column), board.getNWDiagonal(row, column)])
            .filter(arr => arr.length >= 4)
            .flatMap(arr => Observable.range(0, arr.length - 3).map((idx) => [arr[idx], arr[idx + 1], arr[idx + 2], arr[idx + 3]]))
            .filter(arr => this.fourInARow(arr[0], arr[1], arr[2], arr[3], piece.constructor))
            .take(1)
            .map(() => true)
            .defaultIfEmpty(false);
    }

    private fourInARow(p1: Cell, p2: Cell, p3: Cell, p4: Cell, constructor: any): boolean {
        return [p1, p2, p3, p4].every(cell => cell instanceof constructor);
    }
}
